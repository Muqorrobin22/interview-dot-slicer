# About this Project.

Project ini merupakan interview POSISI Slicer untuk PT DOT. Project ini saya hosting [Disini](https://interview-dot-slicer.vercel.app/) sehingga bisa dicek hasilnya secara langsung tanpa harus running.

## Teknologi yang digunakan

- NextJS **Required**
- Tailwind **Required**
- Clsx _Saya gunakan agar code saya lebih mudah dibaca, size nya pun sangat kecil jadi tidak akan banyak berdampak ke performance_

## Milestone

Saya sudah menyelesaikan tugas dengan tepat waktu dan tepat sasaran. komposisi sudah sesuai dengan requirement mulai dari Color, Fonts, Margin, Pixel Perfect. semua aspek sudah saya pertimbangkan dan yang pasti, saya membuatnya per Component dan menerapkan **atomic-components Pattern**

> Responsive Sengaja tidak saya buat karena Responsive ada pertimbangan lain hanya dari sekedar Slicing (Seperti pertimbangan Bisnis dan Product) karena Responsive akan berdampak ke UX.

Adapun Fitur yang saya terapkan mulai dari Carousel (_Saya bikin sendiri_)

# About me.

Saya Achmad Muqorrobin, Berpengalaman dalam dunia Frontend Developer selama 2 tahun lebih dan telah banyak menyelesaikan project seperti ini selama perjalanan karir saya. Kredibilitas saya terbukti melalui Portofolio saya dan bisa diakses [Disini](https://muqorrobin.com)

# Summary

Saya yakin Hasil kerja saya ini sudah cukup mengkomunikasikan kapabilitas saya dalam mengerjakan suatu project dan saya percaya diri saya bisa memberi lebih dari ini apabila memang diberi kesempatan.
Terima Kasih.
