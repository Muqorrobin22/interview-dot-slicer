import { Inter } from "next/font/google";
import "./globals.css";
import localFont from "next/font/local";

export const metadata = {
  title: "Sobat Bangun",
  description: "Bangun rumah dan cari rumah impian dengan Sobat Bagun",
};

const myFont = localFont({
  src: [
    {
      path: "./../public/fonts/SourceSansPro-Light.otf",
      weight: "300",
      style: "light",
    },
    {
      path: "./../public/fonts/SourceSansPro-Regular.otf",
      weight: "400",
      style: "normal",
    },
    {
      path: "./../public/fonts/SourceSansPro-Semibold.otf",
      weight: "600",
      style: "normal",
    },
    {
      path: "./../public/fonts/SourceSansPro-Bold.otf",
      weight: "700",
      style: "normal",
    },
    {
      path: "./../public/fonts/SourceSansPro-Black.otf",
      weight: "800",
      style: "normal",
    },
  ],
});

export default function RootLayout({ children }) {
  return (
    <html lang="en" className={myFont.className}>
      <body>{children}</body>
    </html>
  );
}
