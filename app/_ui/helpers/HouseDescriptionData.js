export const HouseDescriptionData = [
  {
    id: 1,
    title: "Dimensi Tanah",
    luas: "15 x 8m",
    image: "/images/images_symbol-material-width.svg",
  },
  {
    id: 2,
    title: "Luas Bangunan",
    luas: (
      <>
        112m<sup>2</sup>
      </>
    ),
    image: "/images/images_symbol-width-house.svg",
  },
  {
    id: 3,
    title: "Lantai",
    luas: "2",
    image: "/images/images_symbol-stairs.svg",
  },
  {
    id: 4,
    title: "Kamar Tidur",
    luas: "4",
    image: "/images/images_symbol-bedroom.svg",
  },
];
