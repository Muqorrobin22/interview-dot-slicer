export const GalleryTampilanData = [
  {
    id: 1,
    place: "Ruang Keluarga",
    size: "2.0 x 2.9",
    image: "/images/image_tampilan-1.png",
  },
  {
    id: 2,
    place: "Kamar tidur",
    size: "4.0 x 3.4",
    image: "/images/image_tampilan-2.png",
  },
  {
    id: 3,
    place: "Ruang Makan & Dapur",
    size: "3.0 x 2.9",
    image: "/images/image_tampilan-3.png",
  },
  {
    id: 4,
    place: "Ruang Kerja",
    size: "2.0 x 2.9",
    image: "/images/image_tampilan-4.png",
  },
  {
    id: 5,
    place: "Kamar tidur",
    size: "4.0 x 3.4",
    image: "/images/image_tampilan-5.png",
  },
];
