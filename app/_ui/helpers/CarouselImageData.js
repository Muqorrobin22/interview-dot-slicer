export const CarouselImageData = [
  {
    id: 1,
    image: "/images/image_hero-1.png",
    ref: "temp",
  },
  {
    id: 2,
    image: "/images/carousel-2.jpg",
    ref: "temp",
  },
  {
    id: 3,
    image: "/images/carousel-3.jpg",
    ref: "temp",
  },
  {
    id: 4,
    image: "/images/carousel-4.jpg",
    ref: "temp",
  },
];
