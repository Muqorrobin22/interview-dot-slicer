import Image from "next/image";
import { HouseDescriptionStatus } from "./HouseDescriptionsStatus";
import { Button } from "../micro-components/Buttons/Button";

export function DesainLainnyaCard() {
  return (
    <div>
      <div className="cursor-pointer  border-[1px] border-[#E6E6E6] rounded-lg p-[16px] mt-3 w-[316px]">
        <div className=" h-[201px] rounded-lg">
          <Image
            src={`/images/image_desain-lainnya-1.png`}
            alt="hero Image"
            width={0}
            height={0}
            sizes="100vw"
            style={{ width: "100%", height: "100%", borderRadius: "5px" }}
          />
        </div>
        <div className="my-[24px]">
          <div className="flex items-center">
            <h1 className="text-heading-5/bold">Omah Apik 3</h1>
            <h2 className="py-[4px] px-[16px] rounded-3xl bg-[#F1F1F1]  ml-3 ">
              Scandinavian
            </h2>
          </div>
          <div className="flex items-center mt-[12px]">
            <Image
              src={"/images/image_logo-studio.png"}
              width={28}
              height={28}
              alt="logo studio"
            />
            <p className="text-subtitle/regular">Studio SAe</p>
          </div>
          <div className="mt-[16px]">
            <HouseDescriptionStatus />
          </div>
          <div className="mt-[16px]">
            <h1 className="text-subtitle/regular text-custom-black">
              Harga Desain
            </h1>
            <h2 className="text-heading-3/semi-bold text-custom-black my-1">
              Rp. 32.500.000
            </h2>
            <p className="text-subtitle/regular text-custom-gray-808">
              Harga konstruksi mulai dari Rp 560.000.000
            </p>
            <div className="mt-[16px]">
              <Button value={"Konsultasi Sekarang"} outline={true} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
