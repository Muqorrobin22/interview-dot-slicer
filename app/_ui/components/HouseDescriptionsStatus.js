import { HouseDescriptionData } from "../helpers/HouseDescriptionData";
import { HouseDescription } from "../micro-components/Cards/HouseDescription";

export function HouseDescriptionStatus() {
  return (
    <div className="">
      <div className="flex justify-between items-center">
        {HouseDescriptionData.map((data) => (
          <HouseDescription
            key={data.id}
            image={data.image}
            title={data.title}
            luas={data.luas}
          />
        ))}
      </div>
      <div className="mt-[16px] border-[1px] border-[#E6E6E6] "></div>
    </div>
  );
}
