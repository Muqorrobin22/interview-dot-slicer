"use client";

import Image from "next/image";
import { useRef, useEffect, useState } from "react";
import { CarouselImageData } from "../helpers/CarouselImageData";

export function Carousel() {
  const firstImage = useRef(null);
  const secondImage = useRef(null);
  const thirdImage = useRef(null);
  const forthImage = useRef(null);

  const refs = [firstImage, secondImage, thirdImage, forthImage];

  const [activeImage, setActiveImage] = useState(1);

  function GetRefByImageNumber(imageNumber) {
    switch (imageNumber) {
      case 1:
        return firstImage;
      case 2:
        return secondImage;
      case 3:
        return thirdImage;
      case 4:
        return forthImage;
      default:
        return null;
    }
  }

  function scrollToImage(imageNumber) {
    const ref = GetRefByImageNumber(imageNumber);

    if (ref) {
      ref.current.scrollIntoView({
        behavior: "smooth",
        block: "nearest",
        inline: "center",
      });
    }
  }

  //   useEffect(() => {
  //     const intervalId = setInterval(() => {
  //       const nextImage = (activeImage % 4) + 1; // Assuming have 3 images. Adjust Accordingly
  //       setActiveImage(nextImage);
  //       scrollToImage(nextImage);
  //     }, 3000);

  //     return () => clearInterval(intervalId);
  //   }, [activeImage]);

  const handleImageNext = () => {
    setActiveImage((prevIndex) => {
      let nextIndex = (prevIndex + 1) % 5;
      if (nextIndex == 0) nextIndex = 1;
      scrollToImage(nextIndex);
      return nextIndex;
    });
  };

  const handleImagePrev = () => {
    setActiveImage((prev) => {
      let prevIndex = (prev - 1 + 4) % 4;
      if (prevIndex == 0) prevIndex = 4;
      scrollToImage(prevIndex);
      return prevIndex;
    });
  };
  return (
    <section className="relative">
      <div className="absolute top-[50%] translate-y-[-50%] left-[50px] right-[50px] z-10 flex justify-between ">
        <div
          className="bg-custom-white p-[15px] border-[1px] border-custom-red flex items-center justify-center cursor-pointer"
          onClick={handleImagePrev}
        >
          <Image
            src={"/images/carousel_arrow-left.svg"}
            alt="hero Image"
            width={28}
            height={28}
          />
        </div>
        <div
          className="bg-custom-white p-[15px] border-[1px] border-custom-red flex items-center justify-center cursor-pointer"
          onClick={handleImageNext}
        >
          <Image
            src={"/images/carousel_arrow-right.svg"}
            alt="hero Image"
            width={28}
            height={28}
          />
        </div>
      </div>
      <section className="flex m-0 p-0 relative overflow-x-hidden">
        {CarouselImageData.map((data, index) => {
          data.ref = refs[index];

          return (
            <div
              className="relative min-w-[100vw]"
              ref={data.ref}
              key={data.id}
            >
              <Image
                src={data.image}
                alt="hero Image"
                width={0}
                height={0}
                sizes="100vw"
                style={{
                  width: "100%",
                  height: "555px",
                  objectFit: "cover",
                  backgroundSize: "cover",
                }}
              />
            </div>
          );
        })}
      </section>
    </section>
  );
}
