import Image from "next/image";
import { HouseDescriptionStatus } from "./HouseDescriptionsStatus";
import { Button } from "../micro-components/Buttons/Button";

export function CTAKonsultasiCard() {
  return (
    <div className="rounded-lg border-[1px] border-[#E6E6E6] p-[24px]">
      <h1 className="text-heading-4/semi-bold">Omah Apik 3</h1>
      <div className="flex items-center mt-[12px]">
        <Image
          src={"/images/image_logo-studio.png"}
          width={28}
          height={28}
          alt="logo studio"
        />
        <p className="text-subtitle/regular">Studio SAe</p>
      </div>
      <div className="flex mt-[16px]">
        <div>
          <p className="text-subtitle/regular text-custom-gray-4D4">
            Jenis Rumah
          </p>
          <p className="text-subtitle/regular mt-3 text-custom-gray-4D4">
            Tipe Desain
          </p>
        </div>
        <div className="ml-[24px]">
          <p className="text-subtitle/regular text-custom-black">
            Scandinavian
          </p>
          <div className="flex items-center mt-3">
            <Image
              src={"/images/image_check.svg"}
              height={16}
              width={16}
              alt="Dapat dimodifikasi"
            />
            <p className="ml-[8px] text-subtitle/regular text-custom-red ">
              Dapat Dimodifikasi
            </p>
          </div>
        </div>
      </div>
      <div className="w-full h-[1px] bg-[#E6E6E6] mt-[16px]"></div>
      <div className="mt-[16px]">
        <HouseDescriptionStatus />
      </div>
      <div className="mt-[16px]">
        <h1 className="text-subtitle/regular text-custom-black">
          Harga Desain
        </h1>
        <h2 className="text-heading-3/semi-bold text-custom-black my-1">
          Rp. 32.500.000
        </h2>
        <p className="text-subtitle/regular text-custom-gray-808">
          Harga konstruksi mulai dari Rp 560.000.000
        </p>
        <div className="mt-[16px]">
          <Button value={"Konsultasi Sekarang"} fill={true} />
        </div>
      </div>
    </div>
  );
}
