import Image from "next/image";
import Link from "next/link";
import { Button } from "../micro-components/Buttons/Button";

export function Navbar() {
  return (
    <nav className="flex justify-between py-[42px] px-[62px] items-center ">
      <Image src={"/images/DOT_logo.svg"} width={147} height={42} alt="Logo" />
      <section>
        <ul className="flex">
          <li>
            <Link href="#" className="mr-8 text-text/navbar flex items-center">
              {" "}
              <p className="mr-3">Tentang Kami </p>
              <Image
                src={"/images/DOT_arrrow-down.svg"}
                width={20}
                height={20}
                alt="Arrow Down"
              />
            </Link>
          </li>
          <li>
            <Link
              href="#"
              className=" mr-8 text-text/navbar text-custom-red flex  items-center"
            >
              {" "}
              <p className="mr-3">Produk & Layanan </p>
              <Image
                src={"/images/DOT_arrrow-down.svg"}
                width={20}
                height={20}
                alt="Arrow Down"
              />
            </Link>
          </li>
          <li>
            <Link href="#" className="mr-8 text-text/navbar">
              {" "}
              Blog{" "}
            </Link>
          </li>
          <li>
            <Link href="#" className="text-text/navbar">
              {" "}
              FAQ{" "}
            </Link>
          </li>
        </ul>
      </section>
      <section className="flex">
        <div className="mr-3">
          <Button value={"Daftar"} none={true} />
        </div>
        <Button value={"Masuk"} fill={true} />
      </section>
    </nav>
  );
}
