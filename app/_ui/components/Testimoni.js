import Image from "next/image";
export function Testimoni() {
  return (
    <div className="flex items-start ">
      <Image
        src={"/images/image_account-circle.svg"}
        alt="Account Circle"
        width={44}
        height={44}
      />
      <div className="ml-4">
        <div className="flex items-center">
          <h1 className="text-paragraph/semi-bold">Budi Setiadi</h1>
          <p className="text-[#C5C7D0] mx-2"> |</p>
          <div>
            <Image
              src={"/images/image_star.svg"}
              alt="star"
              width={20}
              height={20}
            />
          </div>
          <p className="text-text/stars-text text-custom-blue">4.5</p>
        </div>
        <div>
          <p className="text-paragraph/regular text-custom-gray-666 text-left">
            Desainnya sangat bagus dan pengirimannya cepat. Terima kasih Sobat
            Bangun
          </p>
        </div>
      </div>
    </div>
  );
}
