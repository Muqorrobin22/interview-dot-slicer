import Image from "next/image";
import Link from "next/link";

export function BreadCrumps() {
  return (
    <ul className="flex items-center">
      <li className="mr-3">
        <Link href={"#"} className="flex items-center">
          {" "}
          <p className="text-subtitle/semi-bold text-custom-red mr-2">
            Home
          </p>{" "}
          <Image
            src={"/images/DOT_arrow-right.svg"}
            height={20}
            width={20}
            alt="right"
          />{" "}
        </Link>
      </li>
      <li className="mr-3">
        <Link href={"#"} className="flex items-center">
          <p className="text-subtitle/semi-bold text-custom-red mr-2">
            Layanan Desain{" "}
          </p>
          <Image
            src={"/images/DOT_arrow-right.svg"}
            height={20}
            width={20}
            alt="right"
          />{" "}
        </Link>
      </li>
      <li>
        <Link href={"#"} className="flex items-center">
          <p className="text-subtitle/semi-bold text-custom-gray-999">
            Omah Apik 3{" "}
          </p>
        </Link>
      </li>
    </ul>
  );
}
