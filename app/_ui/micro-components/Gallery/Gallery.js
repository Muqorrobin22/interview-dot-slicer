import Image from "next/image";

export function Gallery({ place, size, image }) {
  return (
    <div className="cursor-pointer [&:not(:last-child)]:mr-10 border-[1px] border-[#E6E6E6] rounded-lg p-[16px] mt-3">
      <div className="w-[260px] h-[201px] rounded-lg">
        <Image
          src={`${image}`}
          alt="hero Image"
          width={0}
          height={0}
          sizes="100vw"
          style={{ width: "100%", height: "100%", borderRadius: "5px" }}
        />
      </div>
      <div className="my-[24px]">
        <h1 className="text-heading-5/bold">{place}</h1>
        <p className="text-subtitle/regular">{size}</p>
      </div>
    </div>
  );
}
