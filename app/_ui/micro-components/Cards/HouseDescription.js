import Image from "next/image";

export function HouseDescription({ title, luas, image }) {
  return (
    <div className="flex items-center justify-center flex-col">
      <Image width={24} height={24} src={`${image}`} alt="logo" />
      <h1 className="text-text/descriptions text-custom-gray-808 mt-1">
        {title}
      </h1>
      <p className="text-subtitle/regular mt-1">{luas}</p>
    </div>
  );
}
