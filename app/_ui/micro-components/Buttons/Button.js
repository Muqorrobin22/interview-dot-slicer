import clsx from "clsx";

export function Button({ value, outline, px = 16, py = 10, none, fill }) {
  return (
    <button
      className={clsx(
        `cursor-pointer py-[${py}px] px-[${px}px] rounded-lg   text-text/button transition-all text-nowrap`,
        none &&
          "bg-none text-custom-black hover:bg-custom-red hover:text-custom-white",
        outline &&
          "border-2 border-custom-red text-custom-red hover:bg-custom-red hover:text-custom-white",
        fill && "bg-custom-red text-custom-white hover:opacity-70"
      )}
      style={{ padding: `${py}px ${px}px`, width: "100%" }}
    >
      {value}
    </button>
  );
}
