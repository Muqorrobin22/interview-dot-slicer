"use client";

import { BreadCrumps } from "@/app/_ui/components/Breadcrumps";
import { Carousel } from "@/app/_ui/components/Carousel";
import { CTAKonsultasiCard } from "@/app/_ui/components/CTAKonsultasiCard";
import { DesainLainnyaCard } from "@/app/_ui/components/DesainLainnyaCard";
import { HouseDescriptionStatus } from "@/app/_ui/components/HouseDescriptionsStatus";
import { Navbar } from "@/app/_ui/components/Navbar";
import { Testimoni } from "@/app/_ui/components/Testimoni";
import { GalleryTampilanData } from "@/app/_ui/helpers/GalleryTampilanData";
import { Button } from "@/app/_ui/micro-components/Buttons/Button";
import { Gallery } from "@/app/_ui/micro-components/Gallery/Gallery";
import Image from "next/image";
import Link from "next/link";
import { useRef, useEffect, useState } from "react";

export default function Home() {
  return (
    <main>
      <Navbar />

      <section className="mx-[62px] my-[24px]">
        <BreadCrumps />
      </section>
      <section className="mb-[24px]">
        <Carousel />
      </section>
      <section className="mx-[62px] my-[32px] flex">
        <div className="basis-9/12">
          <h1 className="text-heading-4/semi-bold">Tampilan Rumah</h1>
          <section className=" mt-[30px]">
            <div className=" flex items-center flex-wrap">
              {GalleryTampilanData.map((data) => (
                <Gallery
                  key={data.id}
                  image={data.image}
                  place={data.place}
                  size={data.size}
                />
              ))}
            </div>
          </section>
        </div>
        <div className="basis-3/12 ">
          <div>
            <CTAKonsultasiCard />
          </div>
          <div>
            <h1 className="text-heading-4/semi-bold mt-[32px] mb-[16px]">
              Testimoni
            </h1>
            <div>
              {[1, 2, 3].map((data) => (
                <div key={data} className="mt-4">
                  <Testimoni />
                </div>
              ))}
            </div>
          </div>
        </div>
      </section>

      <section className="mx-[62px] my-[32px]">
        <h1 className="text-heading-4/semi-bold mb-[24px]">
          Desain Lainnya oleh Studio SAe
        </h1>
        <div className="flex w-auto justify-between">
          {[1, 2, 3, 4].map((data) => (
            <DesainLainnyaCard key={data} />
          ))}
        </div>
      </section>

      <footer className="bg-custom/footer py-[36px] px-[64px] ">
        <section className="flex">
          <div className="basis-6/12">
            <Image
              src={"/images/Logo-footer.svg"}
              width={170}
              height={47}
              alt="Logo footer"
            />
            <p className="text-text/footer-descriptions text-custom-white my-[16px]">
              SobatBangun adalah platform digital dari SIG yang bergerak dengan
              misi mengembangkan proses pembangunan dan renovasi rumah secara
              lebih baik serta berkelanjutan.
            </p>
            <div className="flex mb-[16px]">
              <Image
                src={"/images/footer_Message.svg"}
                alt="message"
                width={24}
                height={24}
              />
              <a
                href="#"
                className="text-custom-white text-text/footer-descriptions underline ml-4"
              >
                sobat@sobatbangum.com
              </a>
            </div>
            <h1 className="text-custom-white text-text/footer-descriptions">
              Sosial Media :
            </h1>
            <div className="flex ml-[-15px]">
              <a href="#" className="inline-block">
                <Image
                  src={"/images/instagram.svg"}
                  alt="instagram"
                  width={100}
                  height={100}
                />
              </a>
              <a href="#" className="inline-block">
                <Image
                  src={"/images/facebook.svg"}
                  alt="instagram"
                  width={100}
                  height={100}
                />
              </a>
              <a href="#" className="inline-block">
                <Image
                  src={"/images/youtube.svg"}
                  alt="instagram"
                  width={100}
                  height={100}
                />
              </a>
            </div>
          </div>
          <div className="text-custom-white text-text/footer-descriptions basis-3/12">
            <h1 className="text-text/footer-heading/semi-bold">
              Produk & Layanan
            </h1>
            <ul>
              <li className="mt-3">
                <a href="#">Renovasi</a>
              </li>
              <li className="mt-3">
                <a href="#">Bangun Rumah</a>
              </li>
              <li className="mt-3">
                <a href="#">Layanan Desain</a>
              </li>
              <li className="mt-3">
                <a href="#">Teknologi Tambahan</a>
              </li>
              <li className="mt-3">
                <a href="#">Beli Material</a>
              </li>
            </ul>
          </div>
          <div className="text-custom-white text-text/footer-descriptions basis-3/12">
            <h1 className="text-text/footer-heading/semi-bold">Tentang Kami</h1>
            <ul>
              <li className="mt-3">
                <a href="#">Tentang SobatBangun</a>
              </li>
              <li className="mt-3">
                <a href="#">Kebijakan Dan Privasi</a>
              </li>
              <li className="mt-3">
                <a href="#">Syarat Dan Ketentuan</a>
              </li>
              <li className="mt-3">
                <a href="#">FAQ</a>
              </li>
              <li className="mt-3">
                <a href="#">Daftar Menjadi Mitra</a>
              </li>
            </ul>
          </div>
        </section>
        <section className="text-paragraph/semi-bold text-custom-white flex justify-between items-center mb-[46px]">
          <div>
            <h1>Kredit Bangun Rumah</h1>
            <Image
              src={"/images/kredit-bangun-rumah.png"}
              alt="krdit bangun rumah"
              width={300}
              height={300}
            />
          </div>
          <div>
            <h1>Tunai Via Bank Transfer</h1>
            <Image
              src={"/images/tunai-via-bank.png"}
              alt="krdit bangun rumah"
              width={300}
              height={300}
            />
          </div>
          <div>
            <h1>Kartu Kredit</h1>
            <Image
              src={"/images/kartu-kredit.png"}
              alt="krdit bangun rumah"
              width={300}
              height={300}
            />
          </div>
          <div>
            <h1>Rekan Teknologi Tambahan</h1>
            <Image
              src={"/images/rekan-teknologi.png"}
              alt="krdit bangun rumah"
              width={300}
              height={300}
            />
          </div>
        </section>
        <section className="text-custom-white text-text/footer-descriptions flex justify-between items-center">
          <div className="flex items-center">
            <p className="mr-4">Powered by :</p>
            <div>
              <Image
                src={"/images/logo-sig.svg"}
                alt="krdit bangun rumah"
                width={71}
                height={41}
              />
            </div>
          </div>
          <div>
            <p className="text-custom-white text-text/footer-copyright">
              Copyright © 2023 SobatBangun. All rights reserved.
            </p>
          </div>
        </section>
      </footer>
    </main>
  );
}
