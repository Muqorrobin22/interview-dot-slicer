/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      colors: {
        "custom-red": "#F5333F",
        "custom-gray-999": "#999999",
        "custom-gray-4D4": "#4D4D4D",
        "custom-gray-666": "#666666",
        "custom-gray-808": "#808080",
        "custom-black": "#000000",
        "custom-white": "#FFFFFF",
        "custom-blue": "#4A5568",
        "custom/footer": "#012846",
      },
      fontSize: {
        "text/navbar": [
          "16px",
          {
            lineHeight: "36px",
            fontWeight: "600",
          },
        ],
        "subtitle/semi-bold": [
          "14px",
          {
            fontWeight: "600",
            lineHeight: "21px",
          },
        ],
        "subtitle/regular": [
          "14px",
          {
            fontWeight: "400",
            lineHeight: "21px",
          },
        ],
        "text/button": [
          "16px",
          {
            fontWeight: "600",
            lineHeight: "20.11px",
          },
        ],
        "heading-3/semi-bold": [
          "32px",
          {
            fontWeight: "600",
            lineHeight: "38.4px",
          },
        ],
        "heading-4/semi-bold": [
          "24px",
          {
            fontWeight: "600",
            lineHeight: "28.8px",
          },
        ],
        "heading-5/bold": [
          "20px",
          {
            fontWeight: "700",
            lineHeight: "24px",
          },
        ],
        "subtitle/regular": [
          "14px",
          {
            fontWeight: "400",
            lineHeight: "21px",
          },
        ],
        "text/descriptions": [
          "12px",
          {
            fontWeight: "400",
            lineHeight: "18px",
          },
        ],
        "text/stars-text": [
          "14px",
          {
            fontWeight: "400",
            lineHeight: "28px",
          },
        ],
        "text/label-text": [
          "14px",
          {
            fontWeight: "400",
            lineHeight: "19.6px",
          },
        ],
        "text/footer-heading/bold": [
          "20px",
          {
            fontWeight: "700",
            lineHeight: "30px",
          },
        ],
        "text/footer-heading/semi-bold": [
          "16px",
          {
            fontWeight: "600",
          },
        ],
        "text/footer-descriptions": [
          "16px",
          {
            fontWeight: "300",
          },
        ],
        "text/footer-copyright": [
          "14px",
          {
            fontWeight: "300",
          },
        ],
        "paragraph/semi-bold": [
          "16px",
          {
            fontWeight: "600",
            lineHeight: "19.2px",
          },
        ],
        "paragraph/regular": [
          "16px",
          {
            fontWeight: "400",
            lineHeight: "21.76px",
          },
        ],
      },
    },
  },
  plugins: [],
};
